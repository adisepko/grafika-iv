import * as THREE from 'three';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);

const cubes = []
for (let i = 0; i < 1000; i++) {
  for (let j = 0; j <10 ; j++) {
    const x = 0.3 * i
    const y = 0.3 * j
    const z = 0.3
    const geometry = new THREE.BoxGeometry(x, y, z);
    const geometry1 = new THREE.BoxGeometry(x, y, z);
    const material = new THREE.MeshBasicMaterial({color: 0xff004cb8});
    const cube = new THREE.Mesh(geometry, material);
    const cube1 = new THREE.Mesh(geometry1, material);
    cube.position.set(0, y, z)
    cube1.position.set(0, y, z)
    cubes.push(cube)
    cubes.push(cube1)
    scene.add(cube);
    scene.add(cube1);
  }
}


camera.position.z = 5;
document.body.appendChild(renderer.domElement);

function animate() {
  requestAnimationFrame(animate);


  renderer.render(scene, camera);
}

animate();
